var fs = require('fs');
var _ = require('lodash');
var httpMitmProxy = require('http-mitm-proxy');

class PGOMITMProxy {
    constructor(options) {
        //console.log(options);
        this.options = options;
        this.reqTaps = [];
        this.resTaps = [];
        this.proxyServer = new httpMitmProxy();
        this.setupProxyHandlers();
        this._reqCount = 0;
        this.domainConfig = {
            'pgorelease.nianticlabs.com': this.handleClientRequest,
            'pokessl.com': this.handleGetSSLCARequest
        };
    }

    setupProxyHandlers() {
        this.proxyServer.onRequest(this.handleIncomingRequest.bind(this));
    }

    listen() {
        this.proxyServer.listen({
            port: this.options.port,
            sslCaDir: this.options.sslCaDir,
            silent: true
        });
    }
    
    tapReq(fn) {
        this.reqTaps.push(fn);
        return this;
    }

    tapRes(fn) {
        this.resTaps.push(fn);
        return this;
    }

    handleClientRequest(ctx, reqcb) {
        //console.log('Incoming Client Call');
        this._reqCount++;
        var reqBuff = [];
        var resBuff =[];
        var reqData = null;
        var resData = null;
        var reqNum = this._reqCount;

        ctx.use(httpMitmProxy.gunzip);

        ctx.onRequestData(function(ctx, chunk, reqDataCallback) {
            //console.log('Incoming request data #%s size:%s', reqNum, chunk.length);
            reqBuff.push(chunk);
            return reqDataCallback(null, null);
        });

        ctx.onRequestEnd(function(ctx, cb) {
            //console.log('Response data end:%s', reqBuff.length);
            reqData = Buffer.concat(reqBuff);
            _.each(this.reqTaps,function(fn) {
                reqData = fn.call(null,reqNum,reqData);
            });
            ctx.proxyToServerRequest.write(reqData);
            return cb();
        }.bind(this));


        ctx.onResponseData(function(ctx, chunk, resDataCallback) {
            //console.log('Incoming response data #%s size:%s', reqNum, chunk.length);
            resBuff.push(chunk);
            return resDataCallback(null, null);
        });

        ctx.onResponseEnd(function(ctx,cb) {
            //console.log('Response data end:%s', resBuff.length);
            resData = Buffer.concat(resBuff);
            _.each(this.resTaps, (fn) => {
                resData = fn.call(null,reqNum,reqData,resData);
            });
            ctx.proxyToClientResponse.write(resData);
            return cb();
        }.bind(this));

        return reqcb();
    }

    handleGetSSLCARequest(ctx) {
        ctx.proxyToClientResponse.writeHead(200,{'Content-type': 'application/x-x509-ca-cert'});
        ctx.proxyToClientResponse.end(fs.readFileSync(this.options.sslCaDir + '/certs/ca.pem'));
    }

    handleIncomingRequest(ctx, reqcb) {
        ctx.proxyToServerRequestOptions.rejectUnauthorized = false;
        if (this.domainConfig[ctx.clientToProxyRequest.headers.host] !== undefined) {
            this.domainConfig[ctx.clientToProxyRequest.headers.host].apply(this, arguments);
        } else {
            return reqcb();
        }
    }
}

module.exports = PGOMITMProxy;