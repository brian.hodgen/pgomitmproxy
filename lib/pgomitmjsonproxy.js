var fs = require('fs');
var _ = require('lodash');
var PGOMITMProxy = require('./pgomitmproxy.js');
var ProtoBuff =  require('node-protobuf');
var pgoProto = new ProtoBuff(fs.readFileSync(__dirname + '/pgoprotos.desc'));

class PGOMITMJSONProxy {
    constructor(options) {
        this.options = options;
        this.requestEnvelopeProto = 'POGOProtos.Networking.Envelopes.RequestEnvelope';
        this.responseEnvelopeProto = 'POGOProtos.Networking.Envelopes.ResponseEnvelope';
        
        this.reqTaps ={};
        this.resTaps = {};
        
        this.proxyInstance = new PGOMITMProxy(this.options);
        this.setupTaps();
    }
    
    listen() {
        this.proxyInstance.listen();
    }
    
    setupTaps() {
        this.proxyInstance.tapReq(this.requestTap.bind(this));
        this.proxyInstance.tapRes(this.responseTap.bind(this));
    };

    tapReq(reqName, fn, modify = false, enabled = true) {
        var reqEnumName = _.toUpper(_.snakeCase(reqName));
        if (!this.reqTaps[reqEnumName]) {
            this.reqTaps[reqEnumName] = [];
        }
        
        this.reqTaps[reqEnumName].push({
            protoName: `POGOProtos.Networking.Requests.Messages.${reqName}Message`,
            enabled: enabled,
            modify: modify,
            fn: fn
        });
        
        return _.last(this.reqTaps[reqEnumName]);
    }

    tapRes(resName, fn, modify = false, enabled = true) {
        var resEnumName = _.toUpper(_.snakeCase(resName));
        if (!this.resTaps[resEnumName]) {
            this.resTaps[resEnumName] = [];
        }

        this.resTaps[resEnumName].push({
            protoName: `POGOProtos.Networking.Responses.${resName}Response`,
            protoReqName: `POGOProtos.Networking.Requests.Messages.${resName}Message`,
            enabled: enabled,
            modify: modify,
            fn: fn
        });
        
        return _.last(this.resTaps[resEnumName]);
    }

    requestTap(reqNum, reqBuff) {
        var reqJSON = pgoProto.parse(reqBuff,this.requestEnvelopeProto);        
        var isRawModified = this.handleRawReqTaps(reqNum,reqJSON);
        var isModified = this.handleReqTaps(reqNum,reqJSON);
        if (isRawModified || isModified) {
            reqBuff = pgoProto.serialize(reqJSON,this.requestEnvelopeProto);
        }
        return reqBuff;
    };
    
    responseTap(reqNum, reqBuff, resBuff) {
        var reqJSON = pgoProto.parse(reqBuff,this.requestEnvelopeProto);
        var resJSON = pgoProto.parse(resBuff,this.responseEnvelopeProto);
        var isRawModified = this.handleRawResTaps(reqNum,reqJSON,resJSON);
        var isModified = this.handleResTaps(reqNum,reqJSON,resJSON);
        if (isModified || isRawModified) {
            resBuff = pgoProto.serialize(resJSON,this.responseEnvelopeProto);
        }
        return resBuff;
    };

    handleRawReqTaps(reqNum,reqJSON) {
        var hasModified = false;
        _.each(this.reqTaps['REQ_ENVELOPE'], (tapObj) => {
            if (tapObj.enabled) {
                tapObj.fn.call(null,reqNum,reqJSON);
                if (tapObj.modify) {
                    hasModified = true;
                }
            }
            
        });
        return hasModified;
    }
    
    handleReqTaps(reqNum,reqJSON) {
        var hasModified = false;
        _.each(reqJSON['requests'], (reqObj) => {
            _.each(this.reqTaps[reqObj['request_type']], (tapObj) => {
                if (tapObj.enabled) {
                    var protoJSON;
                    try {
                        protoJSON = reqObj['request_message'] ? pgoProto.parse(reqObj['request_message'],tapObj.protoName) : {};
                    } catch(err) {
                        protoJSON = {};
                        console.log('Failed to parse request');
                    }
                    tapObj.fn.call(null,reqNum,protoJSON);
                    if (tapObj.modify) {
                        try {
                            reqObj['request_message'] = pgoProto.serialize(protoJSON,tapObj.protoName);
                        } catch (err) {
                            console.log('Failed to modify request');
                        }
                        hasModified = true;
                    }
                }
            });
        });
        return hasModified;                        
    }
    
    handleRawResTaps(reqNum,reqJSON,resJSON) {
        var hasModified = false;
        _.each(this.resTaps['RES_ENVELOPE'], (tapObj) => {
            if (tapObj.enabled) {
                tapObj.fn.call(null,reqNum,reqJSON,resJSON);
                if (tapObj.modify) {
                    hasModified = true;
                }
            }

        });
        return hasModified;
    }
    
    handleResTaps(reqNum,reqJSON,resJSON) {
        var hasModified = false;
        _.each(reqJSON['requests'], (reqObj, idx) => {
            _.each(this.resTaps[reqObj['request_type']], (tapObj) => {
                if (tapObj.enabled) {
                    var protoJSON;
                    var protoReqJSON;
                    try {
                        protoReqJSON = reqObj['request_message'] ? pgoProto.parse(reqObj['request_message'],tapObj.protoReqName) : {};
                        protoJSON = resJSON['returns'][idx] ? pgoProto.parse(resJSON['returns'][idx],tapObj.protoName) : {};
                    } catch(err) {
                        protoJSON = {};                  
                        console.log('Failed to parse response');
                    }
                    tapObj.fn.call(null,reqNum,protoJSON,protoReqJSON);
                    if (tapObj.modify) {
                        try {
                            resJSON['returns'][idx] = pgoProto.serialize(protoJSON,tapObj.protoName);
                            hasModified = true;
                        } catch(err) {
                            console.log('Failed to modify response');
                            hasModified = false;
                        }
                    }
                }  
            });
        });
        return hasModified;
    }
}

module.exports = PGOMITMJSONProxy;