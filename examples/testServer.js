var _ = require('lodash');
var fs = require('fs');
var rand = require('randgen');
var PokeProxy = require('../index.js');
var pokeProxyInstance = new PokeProxy.PGOMITMJSONProxy({
    port: 8888,
    sslCaDir: './ssl-certs'
});

//Force full inventory update
pokeProxyInstance.tapReq('GetInventory',function(reqNum,reqJSON) {
    if (reqJSON) {
        console.log(JSON.stringify(reqJSON,null,'\t'));

    }
    reqJSON['last_timestamp_ms'] = '0';
},true);

pokeProxyInstance.tapRes('GetMapObjects',function(reqNum,resJSON,reqJSON) {
    console.log(JSON.stringify(reqJSON,null,'\t'));
    console.log(JSON.stringify(resJSON,null,'\t'));
});

//Make all player pokemon MEWTWO
pokeProxyInstance.tapRes('GetInventory',function(reqNum,resJSON,reqJSON) {
    console.log(reqJSON);
    console.log('Neat');
    if(_.has(resJSON,'inventory_delta.inventory_items')) {
        _.each(resJSON['inventory_delta']['inventory_items'], (itemObj) => {
            if (_.has(itemObj,'inventory_item_data.pokemon_data')) {
                itemObj['inventory_item_data']['pokemon_data']['pokemon_id'] = 'MEWTWO';
            }
        });
    }
},true);

//Cheat when throwing ball;
pokeProxyInstance.tapReq('CatchPokemon',function(reqNum,reqJSON) {
    reqJSON.normalized_reticle_size = Math.min(1.95, rand.rnorm(1.9, 0.05));
    reqJSON.spin_modifier = Math.min(0.95, rand.rnorm(0.85, 0.1));
    if (reqJSON.hit_pokemon) {
        reqJSON.NormalizedHitPosition = 1.0;
    }
},true);

//Change user display name
pokeProxyInstance.tapRes('GetPlayer', function(reqNum,resJSON,reqJSON) {
    console.log(reqJSON);
    console.log('Neat');
    if (_.has(resJSON,'player_data.username')) {
        resJSON.player_data.username = 'IAmAGod';
    }
},true);

//Basic request logging
pokeProxyInstance.tapReq('REQ_ENVELOPE',function(reqNum,reqJSON) {
    _.each(reqJSON.requests,function(reqObj) {
        console.log('Outgoing Request \'%s\' Length:',reqObj.request_type,reqObj.request_message ? reqObj.request_message.length : 0);
        console.log(reqJSON.latitude,reqJSON.longitude);
    });
    if (_.has(reqJSON,'auth_ticket.start') && _.has(reqJSON,'auth_ticket.end') && _.has(reqJSON,'auth_ticket.expire_timestamp_ms')){
        console.log('ticket start:',reqJSON['auth_ticket']['start'].toString('hex'));
        console.log('ticket end:',reqJSON['auth_ticket']['end'].toString('hex'));
        console.log('ticket expire:',new Date(parseInt(reqJSON['auth_ticket']['expire_timestamp_ms'])));
    } else {
        console.log(reqJSON);
    }
});

//Basic response logging
pokeProxyInstance.tapRes('RES_ENVELOPE',function(reqNum,reqJSON,resJSON) {
    _.each(reqJSON.requests, function(reqObj,idx) {
        console.log('Incoming Response \'%s\' Length:%s',reqObj.request_type, resJSON.returns[idx] ? resJSON.returns[idx].length: 0);
    });

});

pokeProxyInstance.listen();
console.log('Proxy server listening on %s', pokeProxyInstance.options.port);